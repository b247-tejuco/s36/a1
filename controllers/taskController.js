// controller to model path file
const Task= require("../models/Model")


//get all task
module.exports.getAllTasks= () => {
	return Task.find({}).then((result)=> {
		return result;
	})
}

//create new task
module.exports.createTask= (requestDotBody)=> {
	// Creates a task object based on the Mongoose model "taskModel"
	let newTask= new Task({
		name: requestDotBody.name
	})

	return newTask.save().then((task, error)=> {
		if(error){
			console.error(error);
			return false;
		} else {
			return task;
		}
	})
}

// delete a task
module.exports.deleteTask= (taskId)=> {
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=> {
		if(err){
			console.err(err);
			return false;
		} else{
			return removedTask;
		}
	})
}

// update a task
module.exports.updateTask= (taskId, newContent)=> {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false;
		}

		result.name= newContent.name;

		
		return result.save().then((updateTask, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updateTask;
			}
		})
	})
}

// Activity

// get specific task
module.exports.getSpecificTask= (taskId) => {
	return Task.findById(taskId).then((result)=> {
		return result;
	})
}


// update status
module.exports.updateStatus= (taskId)=> {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false;
		}

		result.status= "complete";

		
		return result.save().then((updateTask, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updateTask;
			}
		})
	})
}