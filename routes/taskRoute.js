// setup dependencies
const express= require("express")
const router= express.Router();

// route to controller path file
const taskController= require("../controllers/taskController")


// get all tasks
router.get("/", (request, response)=> {

	// invokes the getAllTasks function from the taskController.js
	taskController.getAllTasks().then(resultFromController=> response.send(resultFromController))
})

// create new task
router.post("/", (request, response)=> {
	taskController.createTask(request.body).then(resultFromController => response.send(resultFromController))
})
// delete a task
router.delete("/:id", (request, response)=> {
	taskController.deleteTask(request.params.id).then(resultFromController=> response.send(resultFromController))
})


// update a task
router.put("/:id", (request, response)=>{
	taskController.updateTask(request.params.id, request.body).then(resultFromController=> response.send(resultFromController));
})

//Activity


// get specific task
router.get("/:id", (request, response)=>{
	taskController.getSpecificTask(request.params.id, request.body).then(resultFromController=> response.send(resultFromController))
})

// update a status of a task
router.put("/:id/complete", (request, response)=>{
	taskController.updateStatus(request.params.id, request.body).then(resultFromController=> response.send(resultFromController));
})


module.exports= router;