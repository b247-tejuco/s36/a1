// npm init -y
// npm install express mongoose@6.10.0


//setup dependencies
const express= require("express");
const mongoose= require("mongoose");

//app to route path file
const taskRoute= require("./routes/taskRoute");

//setup server
const app= express();
const port= 2008;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// mongoDb connection
mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/s36-todo?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", ()=> console.log('Now connected to the database!'));

//general parent route (/tasks)
app.use("/tasks", taskRoute);

//listen port host
app.listen(port, ()=> console.log(`Now listening to port ${port}`));